import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertEquals;

public class Day9Test {

    private static final String DAY_9 = "src/test/resources/day9/day9.txt";

    @InjectMocks
    private Day9 day9 = new Day9();

    @Test
    public void calculateScore_simple() throws Exception {
        assertEquals(1, day9.calculateScore("{}"));
        assertEquals(6, day9.calculateScore("{{{}}},"));
        assertEquals(5, day9.calculateScore("{{},{}}"));
        assertEquals(16, day9.calculateScore("{{{},{},{{}}}}"));
        assertEquals(1, day9.calculateScore("{<a>,<a>,<a>,<a>}"));
        assertEquals(9, day9.calculateScore("{{<ab>},{<ab>},{<ab>},{<ab>}}"));
        assertEquals(9, day9.calculateScore("{{<!!>},{<!!>},{<!!>},{<!!>}}"));
        assertEquals(3, day9.calculateScore("{{<a!>},{<a!>},{<a!>},{<ab>}}"));
    }

    @Test
    public void calculateScore() throws Exception {
        String stream = Util.parseInputFile(DAY_9).get(0);
        int score = day9.calculateScore(stream);
        System.out.println("Day 9 part 1 solution: " + score);
        assertEquals(9251, score);
    }

    @Test
    public void calculateGarbageCount_simple() throws Exception {
        assertEquals(0, day9.calculateGarbageCount("<>"));
        assertEquals(17, day9.calculateGarbageCount("<random characters>"));
        assertEquals(3, day9.calculateGarbageCount("<<<<>"));
        assertEquals(2, day9.calculateGarbageCount("<{!>}>"));
        assertEquals(0, day9.calculateGarbageCount("<!!>"));
        assertEquals(0, day9.calculateGarbageCount("<!!!>>"));
        assertEquals(10, day9.calculateGarbageCount("<{o\"i!a,<{i<a>"));
    }

    @Test
    public void calculateGarbageCount() throws Exception {
        String stream = Util.parseInputFile(DAY_9).get(0);
        int score = day9.calculateGarbageCount(stream);
        System.out.println("Day 9 part 2 solution: " + score);
        assertEquals(4322, score);
    }
}