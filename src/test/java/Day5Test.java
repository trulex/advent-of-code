import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertEquals;


public class Day5Test {

    @InjectMocks
    private Day5 day5 = new Day5();

    @Test
    public void calculateStepCount() {
        Long stepCount = day5.calculateStepCount("src/test/resources/day5/example.txt");

        assertEquals(Long.valueOf(5), stepCount);

        System.out.println("Day 5 solution: " + day5.calculateStepCount("src/test/resources/day5/day5.txt"));
        day5.calculateStepCount("src/test/resources/day5/day5.txt");
    }

    @Test
    public void calculateStepCountPartTwo() throws Exception {
        Long stepCount = day5.calculateStepCountPartTwo("src/test/resources/day5/example.txt");

        assertEquals(Long.valueOf(10), stepCount);

        Long stepCountPartTwo = day5.calculateStepCountPartTwo("src/test/resources/day5/day5.txt");
        System.out.println("Day 5 part 2 solution: " + stepCountPartTwo);
        assertEquals(Long.valueOf(28040648), stepCountPartTwo);
    }
}