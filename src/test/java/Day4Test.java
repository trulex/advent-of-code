import org.junit.Test;
import org.mockito.InjectMocks;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class Day4Test {

    @InjectMocks
    private Day4 day4 = new Day4();

    @Test
    public void isPassphraseValid() {
        assertTrue(day4.isPassphraseValid("aa bb cc dd ee"));
        assertFalse(day4.isPassphraseValid("aa bb cc dd aa"));
        assertTrue(day4.isPassphraseValid("aa bb cc dd aaa"));
    }

    @Test
    public void validPassphraseCount() {
        long validPassphraseCount = day4.validPassphraseCount("src/test/resources/day4/day4.txt");
        System.out.println(validPassphraseCount);
        assertEquals(386, validPassphraseCount);
    }

}