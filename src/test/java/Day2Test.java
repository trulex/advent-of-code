import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertEquals;


public class Day2Test {

    @InjectMocks
    private Day2 day2 = new Day2();

    @Test
    public void calculateChecksum() {
        assertEquals(18, day2.calculateChecksum("src/test/resources/day2/simple.txt"));
        System.out.println(day2.calculateChecksum("src/test/resources/day2/day2.txt"));
        assertEquals(44887, day2.calculateChecksum("src/test/resources/day2/day2.txt"));
    }

    @Test
    public void calculateSumOfQuotients() {
        assertEquals(9, day2.calculateSumOfQuotients("src/test/resources/day2/day2-part2-example.txt"));
        System.out.println(day2.calculateSumOfQuotients("src/test/resources/day2/day2.txt"));
    }
}