import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertEquals;

public class Day8Test {


    private static final String EXAMPLE = "src/test/resources/day8/example.txt";
    private static final String DAY_8 = "src/test/resources/day8/day8.txt";

    @InjectMocks
    private Day8 day8 = new Day8();

    @Test
    public void getLargestRegisterValue_example() throws Exception {
        assertEquals(1, day8.getLargestRegisterValue(EXAMPLE));
    }

    @Test
    public void getLargestRegisterValue() throws Exception {
        int largestRegisterValue = day8.getLargestRegisterValue(DAY_8);
        System.out.println("Day 8 part 1 solution: " + largestRegisterValue);
        assertEquals(4888, largestRegisterValue);
    }

    @Test
    public void getLargestRegisterValueEver_example() {
        assertEquals(10, day8.getLargestRegisterValueEver(EXAMPLE));
    }

    @Test
    public void getLargestRegisterValueEver() {
        int largestRegisterValueEver = day8.getLargestRegisterValueEver(DAY_8);
        System.out.println("Day 8 part 2 solution: " + largestRegisterValueEver);
        assertEquals(7774, largestRegisterValueEver);
    }
}