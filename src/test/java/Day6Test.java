import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertEquals;


public class Day6Test {

    private static final String EXAMPLE = "src/test/resources/day6/example.txt";
    private static final String DAY_6 = "src/test/resources/day6/day6.txt";

    @InjectMocks
    private Day6 day6 = new Day6();

    @Test
    public void calculateRedistributionCount() {
        int example = day6.calculateRedistributionCount(EXAMPLE);
        assertEquals(5, example);

        int redistributionCount = day6.calculateRedistributionCount(DAY_6);
        System.out.println("Day 6 part 1 solution: " + redistributionCount);
        assertEquals(6681, redistributionCount);
    }

    @Test
    public void calculateCycleCount() throws Exception {
        assertEquals(4, day6.calculateCycleCount(EXAMPLE));

        int cycleCount = day6.calculateCycleCount(DAY_6);
        System.out.println("Day 6 part 2 solution: " + cycleCount);
    }

}