import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.*;

public class Day7Test {

    private static final String EXAMPLE = "src/test/resources/day7/example.txt";
    private static final String DAY_7 = "src/test/resources/day7/day7.txt";

    @InjectMocks
    private Day7 day7 = new Day7();

    @Test
    public void getBottomProgramName() throws Exception {
        assertEquals("tknk", day7.getBottomProgramName(EXAMPLE));

        String day7Solution = day7.getBottomProgramName(DAY_7);
        System.out.println("Day 7 solution: " + day7Solution);
        assertEquals("hlqnsbe", day7Solution);
    }

}