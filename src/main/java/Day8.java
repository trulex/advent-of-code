import java.util.*;

/**
 * https://adventofcode.com/2017/day/8
 */
class Day8 {

    private static final String MAX = "register with maximum value EVER";

    int getLargestRegisterValue(String pathToInput) {
        Map<String, Integer> registers = executeInstructions(pathToInput);
        registers.remove(MAX);

        return registers.values().stream().sorted(Collections.reverseOrder()).findFirst().get();
    }

    private Map<String, Integer> executeInstructions(String pathToInput) {
        List<String> instructions = Util.parseInputFile(pathToInput);
        Map<String, Integer> registers = new HashMap<>();
        registers.put(MAX, 0);

        instructions.forEach(instruction -> executeInstruction(registers, instruction));

        return registers;
    }

    private Map<String, Integer> executeInstruction(Map<String, Integer> registers, String instruction) {
        List<String> instructionParts = List.of(instruction.split(" "));
        String register = instructionParts.get(0);
        registers.putIfAbsent(register, 0);
        Integer registerValue = registers.get(register);
        String operator = instructionParts.get(1);
        Integer increment = Integer.valueOf(instructionParts.get(2));
        String comparisonRegister = instructionParts.get(4);
        registers.putIfAbsent(comparisonRegister, 0);
        String comparisonOperator = instructionParts.get(5);
        Integer comparisonValue = Integer.valueOf(instructionParts.get(6));

        boolean isConditionTrue = isConditionTrue(registers, comparisonOperator, comparisonRegister, comparisonValue);
        if (isConditionTrue) {
            int newValue = calculateNewRegisterValue(registerValue, operator, increment);
            registers.put(register, newValue);
            if (newValue > registers.get(MAX)) {
                registers.replace(MAX, newValue);
            }
        }

        return registers;
    }

    private int calculateNewRegisterValue(Integer registerValue, String operator, Integer increment) {
        Operation operation = Operation.valueOf(operator.toUpperCase());
        return operation == Operation.INC ? registerValue + increment : registerValue - increment;
    }

    private boolean isConditionTrue(Map<String, Integer> registers, String comparisonOperator, String comparisonRegister, Integer comparisonValue) {
        Integer comparisonRegisterValue = registers.get(comparisonRegister);

        switch (comparisonOperator) {
            case "<":
                return comparisonRegisterValue < comparisonValue;
            case "<=":
                return comparisonRegisterValue <= comparisonValue;
            case ">":
                return comparisonRegisterValue > comparisonValue;
            case ">=":
                return comparisonRegisterValue >= comparisonValue;
            case "==":
                return Objects.equals(comparisonRegisterValue, comparisonValue);
            case "!=":
                return !Objects.equals(comparisonRegisterValue, comparisonValue);
        }

        return false;
    }

    /**
     * To be safe, the CPU also needs to know the highest value held in any register during this process so that it
     * can decide how much memory to allocate to these operations.
     */
    public int getLargestRegisterValueEver(String pathToInput) {
        Map<String, Integer> registers = executeInstructions(pathToInput);

        return registers.get(MAX);
    }

    private enum Operation {
        INC,
        DEC
    }

}
