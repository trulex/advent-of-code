import java.util.Arrays;
import java.util.OptionalInt;
import java.util.stream.Stream;

/**
 * http://adventofcode.com/2017/day/1
 */
public class Day1 {

    /**
     * Find the sum of all digits that match the next digit in the list.
     * The list is circular, so the digit after the last digit is the first
     * digit in the list.
     * @return
     */
    public int getSum(String digits) {
        int previousDigit = -1;
        int sum = 0;
        char[] chars = digits.toCharArray();

        for (int digit : chars) {
            digit = Character.getNumericValue(digit);
            if (digit == previousDigit) {
                sum += digit;
            }
            previousDigit = digit;
        }
        if (previousDigit == Character.getNumericValue(chars[0])) {
            sum += previousDigit;
        }

        return sum;
    }

}
