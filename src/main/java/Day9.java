import java.util.Stack;

/**
 * https://adventofcode.com/2017/day/9
 */
class Day9 {

    /**
     * Your goal is to find the total score for all groups in your input. Each group is assigned a score which is one
     * more than the score of the group that immediately contains it. (The outermost group gets a score of 1.)
     */
    int calculateScore(String characterStream) {
        Stream stream = new Stream();

        return characterStream
                .chars().mapToObj(x -> (char) x)
                .map(stream::getScore).mapToInt(Integer::intValue)
                .sum();
    }

    int calculateGarbageCount(String characterStream) {
        Stream stream = new Stream();

        return characterStream
                .chars().mapToObj(x -> (char) x)
                .map(stream::getGarbageCount).mapToInt(Integer::intValue)
                .sum();
    }

    private class Stream {

        private boolean ignore;
        private boolean isGarbage;
        private Stack<Character> groupStarts = new Stack<>();

        Integer getScore(Character character) {
            if (ignore) {
                ignore = false;
            } else if (character.equals('<')) {
                isGarbage = true;
            } else if (character.equals('!')) {
                ignore = true;
            } else if (isGarbage) {
                if (character.equals('>')) {
                    isGarbage = false;
                }
            } else if (character.equals('{')) {
                groupStarts.push(character);
            } else if (character.equals('}')) {
                groupStarts.pop();

                return groupStarts.size() + 1;
            }

            return 0;
        }

        Integer getGarbageCount(Character character) {
            if (ignore) {
                ignore = false;
            } else if (character.equals('!')) {
                ignore = true;
            } else if (isGarbage) {
                if (character.equals('>')) {
                    isGarbage = false;
                } else {
                    return 1;
                }
            } else if (character.equals('<')) {
                isGarbage = true;
            } else if (character.equals('{')) {
                groupStarts.push(character);
            } else if (character.equals('}')) {
                groupStarts.pop();
            }

            return 0;
        }
    }

}
