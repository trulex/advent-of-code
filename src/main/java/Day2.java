import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.List;

/**
 * https://adventofcode.com/2017/day/2
 */
class Day2 {

    int calculateChecksum(String pathToInput) {
        List<String> strings = Util.parseInputFile(pathToInput);

        int checksum = strings.stream().map(line -> {
            String[] split = line.split("\t");
            List<Integer> integerList = Arrays.stream(split)
                    .map(Integer::parseInt)
                    .sorted()
                    .collect(Collectors.toList());
            return integerList.get(integerList.size() - 1) - integerList.get(0);
        }).mapToInt(Integer::intValue).sum();

        return checksum;
    }

    /**
     * It sounds like the goal is to find the only two numbers in each row where one evenly divides the other -
     * that is, where the result of the division operation is a whole number.
     * They would like you to find those numbers on each line, divide them, and add up each line's result.
     */
    int calculateSumOfQuotients(String pathToInput) {
        List<String> strings = Util.parseInputFile(pathToInput);

        int sum = strings.stream().map(line -> {
            String[] split = line.split("\t");
            List<Integer> integerList = Arrays.stream(split)
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
            int partialSum = 0;

            for (int i = 0; i < integerList.size(); i++) {
                int current = integerList.get(i);
                for (int j = i + 1; j < integerList.size(); j++) {
                    Integer next = integerList.get(j);
                    int remainder = current % next;

                    if (remainder != 0) {
                        remainder = next % current;
                    }
                    if (remainder == 0) {
                        partialSum = current > next ? current / next : next / current;
                        break;
                    }
                }
            }

            return partialSum;
        }).mapToInt(Integer::intValue).sum();

        return sum;
    }

}
