import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class Util {

    public static List<String> parseInputFile(String pathToInput) {
        List<String> strings = Collections.emptyList();
        try {
            strings = Files.readAllLines(Paths.get(pathToInput));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return strings;
    }

}
