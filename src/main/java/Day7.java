import java.util.*;
import java.util.stream.Collectors;

/**
 * https://adventofcode.com/2017/day/7
 */
class Day7 {

    String getBottomProgramName(String pathToInput) {
        List<String> lines = Util.parseInputFile(pathToInput);
        List<String> discHoldingPrograms = lines.stream().filter(this::holdsADisc).collect(Collectors.toList());
        String bottomProgramName = "";
        List<String> bottomPrograms = new ArrayList<>();

        for (int i = 0; i < discHoldingPrograms.size(); i++) {
            String programName = discHoldingPrograms.get(i).split(" ")[0];
            bottomProgramName = findParent(programName, discHoldingPrograms);
            bottomPrograms.add(bottomProgramName);
        }

        return bottomPrograms.stream().filter(s -> !holdsADisc(s)).findFirst().get();
    }

    private String findParent(String programName, List<String> discHoldingPrograms) {
        Optional<String> parent = discHoldingPrograms.stream().filter(program -> (program.split("->")[1]).contains(programName))
                .findFirst();
        if (parent.isPresent()) {
            return findParent(parent.get(), discHoldingPrograms);
        } else {
            return programName;
        }
    }

    private boolean holdsADisc(String line) {
        return line.contains("->");
    }

}
