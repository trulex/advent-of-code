import java.util.*;
import java.util.stream.Collectors;

/**
 * https://adventofcode.com/2017/day/6
 */
class Day6 {

    int calculateRedistributionCount(String pathToInput) {
        Set<Memory> memorySet = new HashSet<>();
        Memory memory = createMemory(pathToInput);
        MemoryBank max = memory.getBankWithMostBlocks();

        boolean wasAdded = memorySet.add(memory);
        int distributionCount = 0;

        while (wasAdded) {
            distributionCount++;
            memory = memory.distributeBlocks(max);
            wasAdded = memorySet.add(memory);
            max = memory.getBankWithMostBlocks();
        }

        return distributionCount;
    }

    /**
     * Out of curiosity, the debugger would also like to know the size of the loop: starting from a state that has
     * already been seen, how many block redistribution cycles must be performed before that same state is seen again?
     *
     */
    int calculateCycleCount(String pathToInput) {
        Set<Memory> memorySet = new HashSet<>();
        List<Memory> memoryList = new ArrayList<>();
        Memory memory = createMemory(pathToInput);
        MemoryBank max = memory.getBankWithMostBlocks();

        boolean wasAdded = memorySet.add(memory);
        int distributionCount = 0;
        Memory memoryCopy = new Memory();
        memoryCopy.addAll(memory);
        memoryList.add(memoryCopy);

        while (wasAdded) {
            distributionCount++;
            memory = memory.distributeBlocks(max);
            wasAdded = memorySet.add(memory);
            max = memory.getBankWithMostBlocks();

            Memory copy = new Memory();
            copy.addAll(memory);
            memoryList.add(copy);
        }

        return memoryList.size() - memoryList.indexOf(memoryList.get(memoryList.size() - 1)) - 1;
    }

    private Memory createMemory(String pathToInput) {
        String[] strings = Util.parseInputFile(pathToInput).get(0).split("\t");
        List<Integer> collect = Arrays.stream(strings).map(Integer::parseInt).collect(Collectors.toList());
        Memory memory = new Memory();
        memory.addAll(collect);

        return memory;
    }

    private class Memory extends ArrayList<Integer> {

        MemoryBank getBankWithMostBlocks() {
            Memory copy = new Memory();
            copy.addAll(this);
            Collections.sort(copy);
            Integer blockCount = copy.get(copy.size() - 1);
            int position = this.indexOf(blockCount);

            return new MemoryBank(blockCount, position);
        }

        Memory distributeBlocks(MemoryBank max) {
            int blockCount = max.getBlockCount();
            int position = max.getPosition();
            int currentPosition = (position + 1) % size();
            set(position, 0);

            while (blockCount > 0) {
                set(currentPosition, get(currentPosition) + 1);
                blockCount--;
                currentPosition++;
                if (blockCount > 1 && currentPosition == position) {
                    currentPosition++;
                }
                currentPosition = currentPosition % size();
            }

            return this;
        }

    }

    private class MemoryBank {

        private int blockCount;
        private int position;

        MemoryBank(int blockCount, int position) {
            this.blockCount = blockCount;
            this.position = position;
        }

        public int getBlockCount() {
            return blockCount;
        }

        public void setBlockCount(int blockCount) {
            this.blockCount = blockCount;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }

}
