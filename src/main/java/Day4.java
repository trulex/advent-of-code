import java.util.List;

/**
 * https://adventofcode.com/2017/day/4
 */
public class Day4 {

    long validPassphraseCount(String pathToInput) {
        List<String> strings = Util.parseInputFile(pathToInput);

        return strings.stream()
                .filter(this::isPassphraseValid)
                .count();
    }

    boolean isPassphraseValid(String passphrase) {
        List<String> split = List.of(passphrase.split(" "));

        return split.stream()
                .allMatch(string -> split.indexOf(string) == split.lastIndexOf(string));
    }
}
