import java.util.List;
import java.util.stream.Collectors;

/**
 * https://adventofcode.com/2017/day/5
 */
public class Day5 {

    Long calculateStepCount(String pathToInput) {

        return calculateStepCount(pathToInput, 1);
    }

    /**
     * Now, the jumps are even stranger: after each jump, if the offset was three or more,
     * instead decrease it by 1. Otherwise, increase it by 1 as before.
     */
    Long calculateStepCountPartTwo(String pathToInput) {
        return calculateStepCount(pathToInput, 3);
    }

    private Long calculateStepCount(String pathToInput, int factor) {
        List<Integer> offsets = Util.parseInputFile(pathToInput).stream().map(Integer::parseInt).collect(Collectors.toList());
        Long stepCount = 0L;

        int index = 0;
        Integer first = offsets.get(index);
        offsets.set(0, first + 1);
        stepCount++;

        while (true) {
            try {
                Integer next = offsets.get(index + first);
                if (next >= 3 && factor != 1) {
                    offsets.set(index + first, next - 1);
                } else {
                    offsets.set(index + first, next + 1);
                }
                index = index + next;
                stepCount++;
            } catch (IndexOutOfBoundsException e) {
                break;
            }
        }

        return stepCount;
    }

}
